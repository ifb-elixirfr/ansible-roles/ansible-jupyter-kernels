# ansible-jupyter-kernels

This role installs Python and R kernels on JupyterLab installation

## Requirements

You need a JupyperLab installation with a `bin` folder containing the `jupyter-kernelspec` tool.

## Role variables

| Variable | Description |
|----------|-------------|
| `jupyterlab_env_path` | Path of the JupyterLab installation folder |
| `deprecated_kernels` | List of kernels names that needs to be uninstalled |
| `python_kernels` | Python kernel to install or update. Each kernel is a YAML dict with the properties : `name`, `display_name` and `python_path` |
| `r_kernels` | R kernel to install or update. Earch kernel is a YAML dict with teh properties : `name`, `display_name` and `r_path` |

## Example playbook

```yaml
- hosts: all
  vars:
    jupyterlab_env_path: /shared/software/miniconda/envs/jupyterlab-3.5.0
    deprecated_kernels:
        - python2.7
        - python3.7
    python_kernels :
        - name: python3
          display_name: Python 3.12
          python_path: /shared/software/miniconda/envs/python-3.12/bin/python
        - name: python3.9
          display_name: "Python 3.9"
          python_path: /shared/software/miniconda/envs/python-3.9/bin/python
    r_kernels:
        - name: r4.4.1
          display_name: "R 4.4.1"
          r_path: /shared/software/miniconda/envs/r-4.4.1/bin/R
  roles:
    - role: ansible-jupyter-kernels
```

## Licence

GPL-3.0-or-later