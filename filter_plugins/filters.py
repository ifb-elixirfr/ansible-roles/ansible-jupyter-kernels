def remove_last_element(path):
    return '/'.join(path.rstrip('/').split('/')[:-1])

class FilterModule(object):
    def filters(self):
        return {
            'remove_last_element': remove_last_element
        }